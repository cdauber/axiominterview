package com.example.demo;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	private int id;
	private String username;
	@JsonIgnore
	private String password;
	@JsonIgnore
	private byte[] hash;

	/*
	 * Hashes the passcode, stores it in passwordHash, clears password.
	 * Note: This is done before the user is added to the set of users,
	 * so it will not be in the DB with an unhashed password
	 */
	public void hashPasscode() {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			this.hash = hash; 
			password = null;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void setHash(byte[] hash) {
		this.hash = hash;
	}
	public byte[] getHash() {
		return hash;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int idNumber) {
		this.id = idNumber;
	}
	
	@Override
	public String toString() {
	    return new org.apache.commons.lang3.builder.ToStringBuilder(this).
	        append("id", id).
	        append("username", username).
	        append("password", password). //TODO REMOVE FOR DEBUG
	        append("hash", hash). //TODO REMOVE FOR DEBUG
	        toString();
	}
	
	
	
	@Override
	public boolean equals(Object o) {
		
	
		  // If the object is compared with itself then return true   
	    if (o == this) { 
	        return true; 
	    } 
	
	    /* Check if o is an instance of User or not 
	      "null instanceof [type]" also returns false */
	    if (!(o instanceof User)) { 
	        return false; 
	    } 
	      
	    // typecast o to User so that we can compare data members  
	    User c = (User) o; 
	      
	    // Compare the data  and return accordingly  
	    return this.username.equals(c.username);
	} 
	
	@Override
	public int hashCode() {
		return username.hashCode();
	}
}
