package com.example.demo;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	
	/**
	 * Adds one User to the set if the username is not taken
	 * 
	 * @param user - the user to add to the set of all users
	 * @return JSon of the user added
	 * @throws HTTPS Bad Request if Username is taken
	 */
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public ResponseEntity putUser(@RequestBody User user) {
		
		if(Model.setOfUsers.contains(user)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username is taken");
		}
		
		user.setId(Model.getNextFreeId());
		user.hashPasscode();
		
		Model.setOfUsers.add(user);
		return ResponseEntity.ok(user);
	}
	
	/**
	 * @return The JSon representation of all users
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity getAllUsers() {
		return ResponseEntity.ok(Model.getSortedUserList());
	}
	
	
	/**
	 * at /users/id, where id is an integer, return the user with that id number
	 * 
	 * @param paramId the specific user to get
	 * @return the Json repsresenting the user at paramId
	 */
	@RequestMapping(value = "/users/{idInput}", method = RequestMethod.GET)
	public ResponseEntity getSpecificUser(@PathVariable("idInput") int paramId) {
		for(User currentUser : Model.setOfUsers) {
			if(currentUser.getId() == paramId) {
				return ResponseEntity.ok(currentUser);
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User does not exist!");
	}
	
	/**
	 * at /users/id, where id is an integer, delete the user with that id number
	 * 
	 * @param paramId the specific user to delete
	 * @return the Json repsresenting the user at paramId
	 */
	@RequestMapping(value = "/users/{idInput}", method = RequestMethod.DELETE)
	public ResponseEntity deleteSpecificUser(@PathVariable("idInput") int paramId) {
		for(User currentUser : Model.setOfUsers) {
			if(currentUser.getId() == paramId) {
				Model.setOfUsers.remove(currentUser);
				Model.freeNumbers.add(currentUser.getId());
				return ResponseEntity.ok(currentUser);
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User does not exist!");
	}
}
