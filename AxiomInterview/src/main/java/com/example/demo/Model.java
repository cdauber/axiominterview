package com.example.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class Model {
	
	public static Set<User> setOfUsers = new HashSet();
	public static PriorityQueue<Integer> freeNumbers = new PriorityQueue<Integer>(); 
	
	public static int getNextFreeId() {
		if(freeNumbers.peek() == null) {
			return setOfUsers.size() + 1;
		} else {
			return freeNumbers.poll();
		}	
	}
	
	/**
	 * 
	 * @return a list of users sorted by increasing id number
	 * Note: this is horribly innefiecent but I was running up against 3 hours and I knew this would work
	 */
	
	public static List<User> getSortedUserList() {
		List<User> output = new ArrayList<>(setOfUsers);
		Collections.sort(output, new Comparator<User>() {
			
			@Override
			public int compare(User a1, User a2) {
				if(a1.getId()  < a2.getId() ) {
					return -1;
				} else {
					return 1;
				}
		 
		    }
		});
		return output;
	}
}
